import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AgendaService } from 'src/app/services/api/agenda.service';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';

import { forkJoin as observableForkJoin } from 'rxjs';
import { BannerService } from 'src/app/services/api/banner.service';

@Component({
  selector: 'app-agenda-info',
  templateUrl: './agenda-info.component.html',
  styleUrls: ['./agenda-info.component.scss']
})
export class AgendaInfoComponent implements OnInit {

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록
  public agenda: any; // 선택한 Agenda 정보
  public selected: any = {
    date: null,
    room: null
  };
  public banners = []; // 광고배너 리스트

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public agendaService: AgendaService,
    public dateService: DateService,
    public roomService: RoomService,
    private bannerService: BannerService,
  ) {
    this.doInit();
  }

  ngOnInit(): void {
    this.getBanners();
  }

  public doInit() {
    const observables = [this.getAgenda(), this.getDates(), this.getRooms()];
    observableForkJoin(observables).subscribe(res => {
      this.agenda = res[0];
      this.dates = res[1];
      this.rooms = res[2];
      this.selected.date = this.dates.find(date => {
        return date.id === this.route.snapshot.params['dateId']
      });
      this.selected.room = this.rooms.find(room => {
        return room.id === this.route.snapshot.params['roomId']
      });
    });
  }

  /** 아젠다 상세 조회 */
  getAgenda() {
    return this.agendaService.findById(this.route.snapshot.params['agendaId']);
  }

  /** 날짜 목록 조회 */
  getDates() {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms() {
    return this.roomService.find();
  }

  getBanners(): void {
    this.bannerService.find().subscribe(res => {

      res.live.forEach(item => {
        const data = {
          link: item.link,
          thumbImage: item.photoUrl,
          alt: item.title,
        };
        this.banners.push(data);
      });

      // const bannersLiveChat = [];
      // res.liveChat.forEach(item => {
      //   const data = {
      //     link: item.link,
      //     thumbImage: item.photoUrl,
      //     alt: item.title,
      //   };
      //   bannersLiveChat.push(data);
      // });

      // if (bannersLiveChat.length > 0) {
      //   this.bannersLiveChat = bannersLiveChat;
      // }

    });
  }

  /**
   * 날짜 / 룸 선택
   * @param date 
   * @param room 
   */
  setAgendaList(date: any, room: any) {
    this.router.navigate([`/live`], { queryParams: { dateId: date.id, roomId: room.id } });
  }

}
